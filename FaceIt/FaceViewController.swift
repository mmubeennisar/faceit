//
//  ViewController.swift
//  FaceIt
//
//  Created by Mubeen Nisar on 6/16/16.
//  Copyright © 2016 Sleek Solutions. All rights reserved.
//

import UIKit

class FaceViewController: UIViewController {
    
    var expression = FacialExpression(eyes: .Open, eyeBrows: .Relaxed, mouth: .Smile) {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var faceView: FaceView! {
        didSet {
            faceView.addGestureRecognizer(UIPinchGestureRecognizer(target: faceView, action: #selector(FaceView.changeScale(_:))))
            
            let happierGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(FaceViewController.makeHappier))
            happierGestureRecognizer.direction = .Up
            
            let sadderGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(FaceViewController.makeSadder))
            sadderGestureRecognizer.direction = .Down
            
            faceView.addGestureRecognizer(happierGestureRecognizer)
            faceView.addGestureRecognizer(sadderGestureRecognizer)
            
            updateUI()
        }
    }
    
    func makeHappier() {
        expression.mouth = expression.mouth.happierMouth()
    }
    
    func makeSadder() {
        expression.mouth = expression.mouth.sadderMouth()
    }
    @IBAction func tapGesture(sender: UITapGestureRecognizer) {
        if sender.state == .Ended{
        switch expression.eyes {
        case .Open: expression.eyes = .Closed
        case .Closed: expression.eyes = .Open
        case .Squinting: break
        }
        }
    }
    
    private var mouthCurvatures = [FacialExpression.Mouth.Frown: -1.0, .Grin: 0.5 ,.Smile: 1.0, .Smirk: -0.5, .Neutral: 0.0]
    
    private var eyeBrowsTilts = [FacialExpression.EyeBrows.Relaxed:0.5, .Furrowed: -0.5 , .Normal: 0.0]
    
    private func updateUI() {
        if faceView != nil {
            switch expression.eyes {
            case .Open: faceView.eyesOpen = true
            case .Closed: faceView.eyesOpen = false
            case .Squinting: faceView.eyesOpen = false
            }
        
            faceView.mouthCurvature = mouthCurvatures[expression.mouth] ?? 0.0
            faceView.eyeBrowTilt = eyeBrowsTilts[expression.eyeBrows] ?? 0.0
        }
    }
}

